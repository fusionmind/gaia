import { GaiaFeExamplePage } from './app.po';

describe('gaia-fe-example App', () => {
  let page: GaiaFeExamplePage;

  beforeEach(() => {
    page = new GaiaFeExamplePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
